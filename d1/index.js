// SECTION - Objects
/*
	-objects are data types that are used to represent real world objects
	-collection of related data and/or functionalities
	-in javascript, most features like strings and arrays are consdered to be objects


	-their difference in Javascript objects is that the JS object has "key: value" pair
	-keys are also referred to as "properties" while the value is the figure on the right side of the colon

	SYNTAX(using initializers:
	 let/const objectName = {
	
		keyA: valueA,
		keyB: valueB
	 }

*/


let cellphone={
	name: "Nokia 3210",
	manufactureDate: 1999
}

console.log("Result from creating objects using initializers");
console.log(cellphone);
console.log(typeof cellphone);


/*let car={
	name: "batmobile",
	yearReleased: "when Bruce became batman"
}

console.log(car)

*/
// creating objects using constructor function
/*
	-creates a reusable function to create/construct several objects that have same data function
	-instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it



	"this" keyword allowss to assign a new propert for the object by assosciating them with values received from a constructo function's parameter

*/
function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
};

// this is a unique instance/copy of the Laptop object
/*
	"new" keyword creates an instance of an object
	-objects and instances are often interchanged because object literals and instances are distinct/unique
*/
let laptop = new Laptop("Dell", 2012)
console.log("Result from creating objects using initializers");
console.log(laptop);
console.log(typeof laptop);

let laptop2 = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using initializers");
console.log(laptop2);
console.log(typeof laptop2);

let computer = {};
let myComputer = new Object();
console.log("Result from creating objects using initializers");
console.log(myComputer);
console.log(typeof myComputer);


// Accessing objects
/*
	in arrays, we can access the elements inside through the use of square notaton: arrayName[index]
	in javascript it's a bit different

*/

// using dot notation
console.log("Result from dot notation: "+ laptop.name)


// array of objects
let latops = [laptop, laptop2]
console.log(latops[1].name);
// accessing laptops array -laptop1
console.log(latops[0].manufactureDate);
// using string inside the second square bracket would let us access the property inside the object
console.log(latops[0]["manufactureDate"]);




// SECTION - initializing, adding, deleting and reassigning object properties 
/*
	- like any other variables, objects may havae their properties initialized or added after the object was created/declared

	-this is useful for times when an object's properties are undetermined at the time of creating them
*/


let car={};


car.name= "Honda Vios"
car.manufacturedDate = 2022;
console.log(car);

car["manufactured date"]=2019;
console.log(car);

// deleting of properties

delete  car["manufactured date"];
// delete car.manufacturedDate;
console.log(car);


// Reassigning of properties
/*
	change the
*/

car.name = "Dodge Charger R/T";
console.log(car);


// Object methods

let person={
	name: "John",
	talk:function(){
		console.log("Hello! My name is "+ this.name)
	},
	walk: function(){
		console.log(person.name + " walked 25 steps")
	}
}
console.log(person)
console.log("Result from object methods:");
person.talk();
person.walk()


let friend = {
	firName:'"Joe',
	lastName: "Smith",
	address: {
		city: "Austin",
		street:"Texas"
	},
	emails: ["joe@mail.com", "johnHandsome@mail.com"],
	introduce: function(){
		console.log("Hello! My name is "+ this.firName+" "+ this.lastName)
	}


}

friend.introduce();
console.log(friend)


// real-world application of objects

/*
	Scenario
		- we would like to create a game tha
*/

let myPokemon ={
	name: "Pickachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This Pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth");
	},
	ffaint: function(){
		console.log("Pokemon Fainted")
	}
	
};

console.log(myPokemon)

// using constructor function
function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	// Methods

	this.tackle = function(target){
		console.log(this.name+ " tackled " + target.name)
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth")
	};

	this.faint = function(){
		console.log(this.name + " fainted")
	}
}

let charizard = new Pokemon("charizard", 16);
let lechonk = new Pokemon("lechonk", 10);


charizard.tackle(lechonk)

