let trainer={
	name: "Ash Ketchum",
	age: 10,
	pokemon:["Pickachu", "Charizard", "Squirtle", "Balbasaur"],
	friends:{
		hoen: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Squirtle I choose you!")
	}


};

console.log(trainer);
console.log("Result of dot Notaion");
console.log(trainer.name);
console.log("Result of square bracket notation: ");
console.log(trainer["pokemon"]);
console.log("Result of talk Method");
trainer.talk()


function pokemon(name, level){
	this.name = name;
	this.level = level
	this.health = 2*level
	this.attack = level

	// Mehods

	this.tackle = function(target){
		console.log(this.name+ " tackled " + target.name)
		console.log(target.name+ "'s' health is now reduced to " + (target.health - this.attack))

	};

	this.faint = function(){
		console.log(this.name + " fainted")
	}

};

let squirtle = new pokemon("squirtle", 9);
let poochyena = new pokemon("poochyena", 6);
let articuno = new pokemon("articuno", 50);

console.log(squirtle);
console.log(poochyena);
console.log(articuno);

poochyena.tackle(squirtle)
articuno.tackle(poochyena)







